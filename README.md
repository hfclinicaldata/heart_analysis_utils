# heart.analysis.utils
House R code for study of clinical studies of heart failure.

Install and use as a package with:
    install_bitbucket('hfclinicaldata/heart_analysis_utils')

Work making a package  based on:
https://hilaryparker.com/2014/04/29/writing-an-r-package-from-scratch/
http://r-pkgs.had.co.nz/description.html

Create the documentation by running the following commands at an R prompt when working directory is the one this file is in:
library(roxygen2)
library(devtools)
document()

