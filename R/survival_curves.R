
# survival_curves.R 
#
# Creates a slew of pdfs that show survival between treatment arms based on different outcomes.
# input: ../data/all.csv, an analysis matrix with data from many studies
# output: *.pdf
#
# croeder 1/2018  Based on DK's work.


#' plot_study_covar_survival_onscreen
#'
#' @param alldata
#' @param covars
#' @param covarlables
#' @param studies
#' @param outcome_studies
#' @param outcome_days
#' @export
plot_study_covar_survival_onscreen <- function (alldata, covars, covarlabels, studies, outcome_status, outcome_days)
{
  for (h in 1:length(covars)) { 
    plot(survfit(Surv(alldata[,outcome_days]/30, alldata[, outcome_status])~alldata[, covars[h]], conf.type="none"),
        lty=c(1:2), xlim=c(0, 42), ylim=c(0.5, 1))
    title(main=paste('All-cause mortality according to', covarlabels[h], '\n'),
          xlab='Time since enrollment (months)',
          ylab='Proportion event free')
    for (i in 1:length(studies)) {
      plot(survfit(Surv(
                        alldata[alldata$study==studies[i], outcome_days]/30, 
                        alldata[alldata$study==studies[i], outcome_status])
                   ~alldata[alldata$study==studies[i], covars[h]], 
                  conf.type="none"),
           lty=c(1:2), col=i, xlim=c(0,42), ylim=c(0.5,1))
      title(main=paste('All-cause mortality according to', covarlabels[h], '\n', studies[i]),
            xlab='Time since enrollment (months)',
            ylab='Proportion event free')
    }
  }
}

# This one creates separate PDF files.
#' plot_study_covar_survival
#'
#' @param alldata
#' @param covars
#' @param covarlables
#' @param studies
#' @param outcome_studies
#' @param outcome_days
#' @export
plot_study_covar_survival <- function (alldata, covars, covarlabels, studies, outcome_status, outcome_days)
{
  for (h in 1:length(covars)) { 
    pdf(paste("All-cause_mortality_for_", covarlabels[h], "_all_studies.pdf", sep=""))
    plot(survfit(Surv(alldata[,outcome_days]/30, alldata[, outcome_status])~alldata[, covars[h]], conf.type="none"),
        lty=c(1:2), xlim=c(0, 42), ylim=c(0.5, 1))
    title(main=paste('All-cause mortality according to', covarlabels[h], '\n'),
          xlab='Time since enrollment (months)',
          ylab='Proportion event free')
    dev.off() 
    for (i in 1:length(studies)) {
      pdf(paste("All-cause_mortality_for_", covarlabels[h], "_in_ ", studies[i], ".pdf", sep=""))
      plot(survfit(Surv(
                        alldata[alldata$study==studies[i], outcome_days]/30, 
                        alldata[alldata$study==studies[i], outcome_status])
                   ~alldata[alldata$study==studies[i], covars[h]], 
                  conf.type="none"),
           lty=c(1:2), col=i, xlim=c(0,42), ylim=c(0.5,1))
      title(main=paste('All-cause mortality according to', covarlabels[h], '\n', studies[i]), 
            xlab='Time since enrollment (months)',
            ylab='Proportion event free')
      dev.off()
    }
  }
}


